package orm

import (
	"fmt"
	"github.com/coreos/etcd/pkg/fileutil"
	"gitlab.com/drakonka/roee/lib"
	"gitlab.com/drakonka/roee/lib/abm/aggregate"
	"gitlab.com/drakonka/roee/lib/abm/world"
	"io/ioutil"
	"os"
	"path"
	"path/filepath"
	"reflect"
	"runtime"
	"sync"
)

type aggregationModifier struct {
	wg               *sync.WaitGroup
	projectRootDir   string
	mainFile         string
	agentLocationDir string
	reifiedEntities  []reifiedEntity
	requiredIncludes []string
}

func (m *aggregationModifier) Init() {
	m.projectRootDir = m.findProjectRoot()
	m.mainFile = fmt.Sprintf("%s/cmd/roeesim/main.go", m.projectRootDir)
	m.agentLocationDir = fmt.Sprintf("%s/lib/abm/aggregate", m.projectRootDir)
	fileutil.TouchDirAll(m.agentLocationDir)
}

func (m *aggregationModifier) AddReifiedEntity(re reifiedEntity) {
	m.reifiedEntities = append(m.reifiedEntities, re)
}

func (m *aggregationModifier) findProjectRoot() string {
	_, b, _, _ := runtime.Caller(0)
	bp := filepath.Dir(b)
	dir := path.Join(bp, "../../")
	return dir
}

func (m *aggregationModifier) Do(wg *sync.WaitGroup) error {
	for _, re := range m.reifiedEntities {
		newEmergentTypeName := re.Name()

		fName := fmt.Sprintf("%s.go", newEmergentTypeName)
		newEmergentTypeFullPath := fmt.Sprintf("%s/%s", m.agentLocationDir, fName)

		var aggregate *world.Aggregate
		t := reflect.TypeOf(aggregate).Elem()
		s := m.implementInterface(newEmergentTypeName, "aggregate", t)

		err := ioutil.WriteFile(newEmergentTypeFullPath, []byte(s), 0777)

		if err != nil {
			fmt.Println(err)
		}

		f := lib.OpenFile(newEmergentTypeFullPath)
		implementFields(f)
		m.implementInit(f, re.Name())
		m.implementQualify(f, re.Name())
		m.implementDestroy(f, re.Name())
		m.implementAddElement(f, re.Name())
		m.implementSetElements(f, re.Name())
		m.implementElementKeys(f, re.Name())
		m.implementRemoveElement(f, re.Name())
		m.instantiateQualifier(re.Name(), re.Qualifiers())
		addImports(f, []string{"sync"})
		f.Close()
	}
	wg.Done()
	return nil
}

func (m *aggregationModifier) implementSetElements(f *os.File, name string) {
	method := fmt.Sprintf(`func (a *%s) SetElements(elements []interface {})  {
		for _, k := range a.ElementKeys() {
			var keep bool
			for idx, nE := range elements {
				if nE == k {
					keep = true
					elements = append(elements[:idx], elements[idx+1:]...)
					break
				}
			}
			if !keep {
				a.RemoveElement(k)
			}
		}
		for _, e := range elements {
			a.AddElement(e)
		}
	}
`, name)
	implementMethod(f, "SetElements", method)
}

func (m *aggregationModifier) implementElementKeys(f *os.File, name string) {
	method := fmt.Sprintf(`func (a *%s) ElementKeys() []interface{}  {
		var keys []interface{}
		a.lock.RLock()
		for k, _ := range a.Elements {
			keys = append(keys, k)
		}
		a.lock.RUnlock()
		return keys
	}
`, name)
	implementMethod(f, "ElementKeys", method)
}

func (m *aggregationModifier) implementAddElement(f *os.File, name string) {
	method := fmt.Sprintf(`func (a *%s) AddElement(e interface{}) {
	a.lock.Lock()
	if a.Elements == nil {
		a.Elements = make(map[string]*world.Listener)
	}
	uid := e.(string)
	agent := a.world.Population[uid]
	if agent != nil {
		l := agent.AddListener()
		a.Elements[uid] = l
		go a.listen(l)
	}
	a.lock.Unlock()
}
`, name)
	implementMethod(f, "AddElement", method)
	m.implementListen(f, name)

}

func (m *aggregationModifier) implementRemoveElement(f *os.File, name string) {
	method := fmt.Sprintf(`func (a *%s) RemoveElement(e interface{}) {
	uid := e.(string)
	exUids := a.ElementKeys()
	for _, exUid := range exUids {
		exUidStr := exUid.(string)
		if exUidStr == uid {
			a.lock.RLock()
			listener := a.Elements[exUidStr]
			a.lock.RUnlock()
			if listener != nil && !listener.Closed {
				select {
				case listener.C <- "removed":
				default:
				}
				listener.Destroy()
			}
			a.lock.Lock()
			delete(a.Elements, exUidStr)
			numElements := len(a.Elements)
			a.lock.Unlock()

			if numElements <= 1 && a.Age > -1 {
				a.Destroy()
			}
			break
		}
	}
}
`, name)
	implementMethod(f, "RemoveElement", method)
}

func (m *aggregationModifier) implementListen(f *os.File, name string) {

	method := fmt.Sprintf(`func (a *%s) listen(l *world.Listener) {
    for {
		select {

		case val := <- l.C:
			if val == nil {
				return
			}
			msg := val.(string)
			fmt.Println("Got message: " + msg)
			switch msg {
			case "death":
				fmt.Println("Got death message from " + l.Name)
				a.RemoveElement(l.Name)
				return
			case "removed":
				fmt.Println(a.Name + " ( " + string(a.Id) + ") Got removed message from " + l.Name)
				return
			default:
				fmt.Println("Message was " + msg)
				return
			}
		}
	}
		fmt.Println("Sleeping")
		time.Sleep(time.Second)
	}
`, name)
	implementMethod(f, "listen", method)
	addImports(f, []string{"fmt", "time"})

}

func (m *aggregationModifier) implementInit(f *os.File, name string) {
	method := fmt.Sprintf(`func(a *%s) Init(w *world.Grid) {
		a.Name = "%s"
		a.world = w
		w.Aggregates = append(w.Aggregates, a)
		a.lock = &sync.RWMutex{}
	}`, name, name)

	implementMethod(f, "Init", method)
}

func (m *aggregationModifier) implementQualify(f *os.File, name string) {
	method := fmt.Sprintf(`func(a *%s) Qualify(uid interface{}) bool {
		var qualified bool
		for _, q := range a.Qualifiers {
			for _, e := range a.ElementKeys() {
				// We don't want to qualify an agent against itself...
				if e == uid {
					continue
				}
				qualified = q.QualifyAgent(e, uid)
				if qualified {
					a.AddElement(uid)
					q.MergeAdjacentGroups(uid)
					break
				} else  {
					a.lock.RLock()
					_, ok := a.Elements[uid.(string)]
					a.lock.RUnlock()
					if ok {
						a.RemoveElement(uid)
						break
					}
				}
			}
		}
		if a.Elements == nil || len(a.Elements) == 0 {
			a.Destroy()
			return false
		}
		return qualified
	}`, name)

	implementMethod(f, "Qualify", method)
}

func (m *aggregationModifier) implementDestroy(f *os.File, name string) {
	method := fmt.Sprintf(`func(a *%s) Destroy() {
		fmt.Printf("\n\nDestroying Aggregate %d", a.Id)
		a.Age = -1
		a.world.Lock.Lock()
		idx := getGroupIdxById(a.Id, a.world.Aggregates)
		if idx > -1 {
			fmt.Printf("\nFound aggregate to destroy %!d(MISSING)", a.Id)

			a.world.Aggregates = append(a.world.Aggregates[:idx], a.world.Aggregates[idx+1:]...)
			for _, key := range a.ElementKeys() {
				a.RemoveElement(key)
			}
		}
		a.world.Lock.Unlock()
	}`, name)

	implementMethod(f, "Destroy", method)
}

func (m *aggregationModifier) instantiateQualifier(name string, qq []world.Qualifier) error {
	var qualifierDefs []string
	for _, q := range qq {
		aq := q.(*aggregate.AggregateQualifier)
		typeToInstantiate := fmt.Sprintf("reflect.TypeOf(aggregate.%s{})", name)
		qd := fmt.Sprintf("\n&aggregate.AggregateQualifier{FieldName: \"%s\",MinDist: %f,MaxDist: %f,MinQuant: %d,MaxQuant: %d,TypeToInstantiate: %s}", aq.FieldName, aq.MinDist, aq.MaxDist, aq.MinQuant, aq.MaxQuant, typeToInstantiate)
		qualifierDefs = append(qualifierDefs, qd)
	}
	f := lib.OpenFile(m.mainFile)
	addImports(f, []string{"gitlab.com/drakonka/roee/lib/abm/aggregate"})
	addToSlice(f, "qualifiers", qualifierDefs)
	f.Close()
	return nil
}
