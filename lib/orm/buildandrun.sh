#!/bin/sh -x

echo "building and running roee"
echo "Current dir" $PWD
go version

echo "running gofmt"
gofmt -s -w lib

echo "compiling"
cd cmd/roeesim;
go build

GOBIN=$GOPATH/bin go install

roeesim & disown roeesim