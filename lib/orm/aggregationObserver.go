package orm

import (
	"fmt"
	"gitlab.com/drakonka/roee/lib"
	"gitlab.com/drakonka/roee/lib/abm/aggregate"
	"gitlab.com/drakonka/roee/lib/abm/world"
	"math"
	"reflect"
	"sort"
	"strings"
	"sync"
)

// This is a boostrap Observer which looks for patterns in agent properties and aggregates them if found
// Eg - agents which are close enough together by distance may be a "flock"

/* Questions:
1) How does it look at the data? In real time or by reading the json?
	First instinct is by reading the json every n ticks.
2) How does it get all properties?
	Reflection
3) How does it find out when a property is *close enough* to some other set of properties in other agents?
	Find median of all properties maybe?
4) How much data is enough to establish a pattern? How many data points do we NEED?
	how many ticks do we expect in total?
*/

type AggregationObserver struct {
	potentialAggregates map[string][]float64
	reifier             Reifier
	c                   chan world.Grid
	ticksProcessed      int
	wg                  *sync.WaitGroup
}

func (e *AggregationObserver) GridChan() chan world.Grid {
	return e.c
}

func (e *AggregationObserver) NextStep() Reifier {
	return e.reifier
}

func (e *AggregationObserver) Init(wg *sync.WaitGroup) {
	e.wg = wg
	e.potentialAggregates = make(map[string][]float64)
	e.c = make(chan world.Grid) // Use an unbuffered channel
	e.reifier = &aggregationReifier{}
	e.reifier.Init()
	// Infinite loop listening for channel
	go func() {
		fmt.Println("\n\nStarting aggregation observer")
		for {
			select {
			case g := <-e.c:
				e.Watch(g)
				if g.RemainingTicks == 1 {
					fmt.Printf("\nLast tick! Ticks simulated in grid: %d, ticks processed by observer: %d", g.Tick, e.ticksProcessed)
					e.analyzePotentials()
					e.wg.Done()
				}
			}
		}
	}()
}

func (e *AggregationObserver) analyzePotentials() {
	// Find potential with the most agents.
	for propName, distances := range e.potentialAggregates {
		fmt.Printf("\nFinding aggregates for property %s", propName)
		// Find mode
		modeDist := -1.00
		buckets := make(map[float64]int)
		for _, f := range distances {
			buckets[f]++
		}

		for d, count := range buckets {
			if modeDist == -1.00 {
				modeDist = d
				continue
			}
			if count > buckets[modeDist] {
				modeDist = d
			}
		}

		newQualifier := aggregate.AggregateQualifier{
			FieldName: propName,
			MinDist:   -1.00,
			MaxDist:   modeDist,
			MinQuant:  -1,
			MaxQuant:  -1}

		e.reifier.AddParam(newQualifier)

		fmt.Printf("\nAgents of distance <= %f are aggregates", modeDist)
	}
}

func (e *AggregationObserver) Watch(grid world.Grid) {
	if len(grid.Population) == 0 {
		return
	}

	var fieldNames []string
	// find fields to check
	for _, a := range grid.Population {
		t := reflect.TypeOf(a).Elem()

		for i := 0; i < t.NumField(); i++ {
			fieldNames = lib.UnionStr(fieldNames, []string{t.Field(i).Name})
		}
	}
	for _, fn := range fieldNames {
		firstC := string(fn[0])
		if strings.ToLower(firstC) == firstC {
			continue
		}
		proximities := aggregate.GetProximities(grid.Population, fn)
		var vals []float64
		for _, p := range proximities {
			vals = append(vals, p.Distance)
		}
		sort.Float64s(vals)
		// Try to find "near" for this set of values. It needs to be less than the median
		near := e.findNear(vals)
		if near != -999 {
			e.potentialAggregates[fn] = append(e.potentialAggregates[fn], near)
		}
	}

	e.ticksProcessed++
	fmt.Println("\nDone with watch")
}

func (e *AggregationObserver) getMedianDistance(sortedDistances []float64) float64 {
	medianIdx := float64(len(sortedDistances)) / 2.00
	if medianIdx == float64(int64(medianIdx)) {
		return sortedDistances[int(medianIdx)]
	}
	lowIdx := int(math.Trunc(medianIdx))
	median := (sortedDistances[lowIdx] + sortedDistances[lowIdx+1]) / 2
	return median
}

func (e *AggregationObserver) findNear(sortedDistances []float64) float64 {
	var near float64
	med := e.getMedianDistance(sortedDistances)

	smallestDist := sortedDistances[0]
	// smallestDist can never be GREATER than med here, but anyway...
	if smallestDist >= med {
		return -999
	}
	// how many do we *need* for it to be a flock?
	near = smallestDist
	return near
}

func (e *AggregationObserver) countOccurrences(target float64, sortedDistances []float64) int {
	var res int
	for _, d := range sortedDistances {
		if d == target {
			res++
		} else if d > target {
			break
		}
	}
	return res
}
