package world

import (
	"fmt"
	"math/rand"
	"time"

	"reflect"
	"sync"
)

var reporter Reporter
var InstructionTypes []reflect.Type
var AgentTypes []reflect.Type
var Qualifiers []Qualifier

// var Aggregates []Aggregate

var World *Grid

type Grid struct {
	X              int
	Y              int
	Population     map[string]Agent
	Aggregates     []Aggregate
	Tick           int
	RemainingTicks int
	startTime      int64
	obsChans       []chan Grid
	Lock           *sync.RWMutex
}

func NewGrid(x, y int, obsChans []chan Grid) *Grid {
	g := Grid{X: x, Y: y}
	g.obsChans = obsChans
	g.Lock = &sync.RWMutex{}
	return &g
}

func getRandomAgentType() reflect.Type {
	idx := 0
	if len(AgentTypes) > 1 {
		idx = rand.Intn(len(AgentTypes))
	}
	t := AgentTypes[idx]
	return t
}

func getRandomInstructionType() reflect.Type {
	idx := 0
	if len(InstructionTypes) > 1 {
		idx = rand.Intn(len(InstructionTypes))
	}
	t := InstructionTypes[idx]
	return t
}

func (g *Grid) Populate(num int) {

	g.Population = make(map[string]Agent)
	for i := 0; i < num; i++ {

		at := getRandomAgentType()
		agent := reflect.New(at).Interface().(Agent)

		it := getRandomInstructionType()
		ins := reflect.New(it).Interface().(Instruction)
		ins.Init()
		agent.Init(ins)
		posX := rand.Intn(g.X)
		posY := rand.Intn(g.Y)
		pos := Pos{X: posX, Y: posY}
		agent.SetId(i)
		agent.SetPos(pos)
		g.AddAgent(agent)
	}
}

func (g *Grid) AddAgent(a Agent) {
	x := a.GetPos().X
	y := a.GetPos().Y
	g.Lock.Lock()
	g.Population[fmt.Sprintf("%d,%d", x, y)] = a
	g.Lock.Unlock()
}

func (g *Grid) DeleteAgent(p Pos) {
	k := fmt.Sprintf("%d,%d", p.X, p.Y)
	g.Lock.Lock()
	if a, ok := g.Population[k]; ok {
		delete(g.Population, k)
		a.Destroy()
	}
	g.Lock.Unlock()
}

func (g *Grid) Run(ticks int) {
	g.RemainingTicks = ticks
	reporter = NewReporter("")
	g.startTime = time.Now().Unix()
	for i := ticks; i > 0; i-- {
		reporter.reportTick(g)
		g.processTick()
	}
	reporter.finalizeReporter()
}

func (g *Grid) processTick() {
	g.Tick++
	fmt.Printf("\nTick: %d", g.Tick)
	g.processAgents()
	g.runQualifiers()
	g.updateChannels()
	g.RemainingTicks--
}

func (g *Grid) runQualifiers() {
	//	defer lib.TimeElapsed("runQualifiers", time.Millisecond * 100)()
	for _, q := range Qualifiers {
		q.Qualify(nil)
	}
}

func (g *Grid) updateChannels() {
	dc := *g
	dc.Population = make(map[string]Agent)
	for k, v := range g.Population {
		dc.Population[k] = v
	}
	for _, c := range g.obsChans {
		c <- dc
	}
}

func (g *Grid) processAgents() {
	// defer lib.TimeElapsed("processAgents()", time.Second * 2)()
	for uid, a := range g.Population {
		a.Do()
		g.qualifyAggregates(uid)
	}
}

func (g *Grid) qualifyAggregates(uid string) {
	// defer lib.TimeElapsed("qualifyAggregates", time.Second * 2)()
	for _, ag := range g.Aggregates {
		ag.Qualify(uid)
	}
}
