package world

import (
	"fmt"
	"sync"
)

type EncodeableChannel chan interface{}

func (c *EncodeableChannel) MarshalJSON() (b []byte, err error) {
	b = []byte("{}")
	return b, err
}

type Listener struct {
	Name   string
	C      EncodeableChannel
	b      *Broadcaster
	Closed bool
}

func (l *Listener) Destroy() {
	if l.Closed {
		return
	}
	l.Closed = true
	l.b.DestroyListener(l)
}

type Broadcaster struct {
	listeners []*Listener
	m         *sync.RWMutex
}

func (b *Broadcaster) AddListener() *Listener {
	l := Listener{C: make(chan interface{}), b: b}
	if b.m == nil {
		b.m = &sync.RWMutex{}
	}
	b.m.Lock()
	b.listeners = append(b.listeners, &l)
	b.m.Unlock()
	return &l
}

func (b *Broadcaster) DestroyListener(l *Listener) {
	b.m.Lock()
	fmt.Printf("\nHad %d listeners\n", len(b.listeners))
	for idx, l2 := range b.listeners {
		if l2 == l {
			b.listeners = append(b.listeners[:idx], b.listeners[idx+1:]...)
			break
		}
	}
	if !l.Closed {
		close(l.C)
		l = nil
	}
	fmt.Printf("\nNow have %d listeners\n", len(b.listeners))
	b.m.Unlock()

}

func (b *Broadcaster) Broadcast(msg interface{}) {
	if b.listeners == nil {
		return
	}
	fmt.Printf("\nBroadcasting %v to %d listeners\n", msg, len(b.listeners))
	for _, l := range b.listeners {
		if l.Closed {
			continue
		}
		select {
		case l.C <- msg:
			fmt.Println("sent message", msg)
		default:
			fmt.Println("no message sent")
		}
	}
}
