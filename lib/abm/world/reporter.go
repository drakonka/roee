package world

import (
	"encoding/json"
	"fmt"
	"github.com/coreos/etcd/pkg/fileutil"
	"gitlab.com/drakonka/roee/lib"
	"gitlab.com/drakonka/roee/lib/abm"
	"os"
	"path"
	"path/filepath"
	"runtime"
	"time"
)

type Reporter interface {
	initReporter()
	reportTick(g *Grid)
	finalizeReporter()
}

type SimpleReporter struct {
	filePath string
	dataDir  string
	dataName string
	file     *os.File
}

func NewReporter(f string) Reporter {
	sr := SimpleReporter{}
	if len(f) == 0 {
		_, b, _, _ := runtime.Caller(0)
		bp := filepath.Dir(b)
		dir := path.Join(bp, "../../orm/data/")
		dataName := fmt.Sprintf("%d", time.Now().Unix())
		dataDir := path.Join(dir, dataName)
		sr.dataDir = dataDir
		sr.dataName = dataName
		fileutil.TouchDirAll(sr.dataDir)
	}
	return &sr
}

func (r *SimpleReporter) initReporter() {
	r.file.WriteString("{ \"gridStates\": [")
}

func (r *SimpleReporter) finalizeReporter() {
	dirPath := "/var/www/html/roee/data/" + r.dataName
	err := abm.CopyDir(r.dataDir, dirPath)
	os.Chmod(dirPath, os.ModePerm)
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println("Finalized reporter")
	r.file.Close()
}

func (r *SimpleReporter) reportTick(g *Grid) {
	filePath := path.Join(r.dataDir, fmt.Sprintf("%d", g.Tick)+".json")
	f := lib.MakeAndOpenFile(filePath)
	j, err := json.Marshal(*g)
	if err != nil {
		panic(err)
	}
	f.WriteString(string(j))
	f.Close()
}
