package aggregate

import (
	"gitlab.com/drakonka/roee/lib"
	"gitlab.com/drakonka/roee/lib/abm/world"
	"reflect"
	"time"
)

type AggregateQualifier struct {
	FieldName         string
	MinDist           float64
	MaxDist           float64
	MinQuant          int
	MaxQuant          int
	TypeToInstantiate reflect.Type
	world             *world.Grid
}

func (q *AggregateQualifier) Init(w *world.Grid) {
	q.world = w
}

func (q *AggregateQualifier) Qualify(agents map[string]world.Agent) []world.Aggregate {
	aggregates := q.sortIntoAggregates(agents, true)
	return aggregates
}

func (q *AggregateQualifier) QualifyAgent(a1uid, a2uid interface{}) bool {
	q.world.Lock.RLock()
	a1 := q.world.Population[a1uid.(string)]
	a2 := q.world.Population[a2uid.(string)]
	q.world.Lock.RUnlock()
	if a1 == nil || a2 == nil {
		return false
	}
	proximity := GetProximity(a1, a2, q.FieldName)

	if q.MaxDist == -1 || proximity.Distance <= q.MaxDist {
		if q.MinDist == -1 || proximity.Distance >= q.MinDist {
			return true
		}
	}
	return false
}

func (q *AggregateQualifier) sortIntoAggregates(agents map[string]world.Agent, doMakeNewAggregates bool) []world.Aggregate {
	var agentsToSort map[string]world.Agent
	var newAggregates []world.Aggregate
	if agents == nil {
		agentsToSort = q.world.Population
		newAggregates = q.world.Aggregates
	} else {
		agentsToSort = agents
	}

	proximities := GetProximities(agentsToSort, q.FieldName)
	for _, proximity := range proximities {
		if q.MaxDist == -1 || proximity.Distance <= q.MaxDist {
			if q.MinDist == -1 || proximity.Distance >= q.MinDist {
				// check if either of these agents is already in any of the aggregate groups.
				// If so, add other gent to the same group
				var agentsInGroup bool
				for _, group := range newAggregates {
					if group.GetName() != q.TypeToInstantiate.Name() {
						continue
					}
					elements := group.ElementKeys()
					a1IsInGroup := agentIsInAggregateGroup(elements, proximity.a1)
					a2IsInGroup := agentIsInAggregateGroup(elements, proximity.a2)
					if a1IsInGroup && a2IsInGroup {
						agentsInGroup = true
						break
					} else if a1IsInGroup && !a2IsInGroup {
						qualified := group.Qualify(proximity.a2.GetUniqueId())
						if qualified {
							agentsInGroup = true
							break
						}
					} else if !a1IsInGroup && a2IsInGroup {
						qualified := group.Qualify(proximity.a1.GetUniqueId())
						if qualified {
							agentsInGroup = true
							break
						}
					}
				}

				// If the agents were not handled as part of an existing aggregate,
				// create a new aggregate
				if !agentsInGroup {
					// Create new group
					inst := reflect.New(q.TypeToInstantiate).Interface()
					aggregate := inst.(world.Aggregate)
					var elements []interface{}
					elements = append(elements, proximity.a1.GetUniqueId())
					elements = append(elements, proximity.a2.GetUniqueId())
					if agents == nil {
						aggregate.Init(q.world)
					}
					aggregate.SetElements(elements)
					aggregate.SetId(int(time.Now().UnixNano()))
					aggregate.SetQualifiers([]world.Qualifier{q})
					newAggregates = append(newAggregates, aggregate)
				}
			}
		}
	}
	return newAggregates
}

func agentIsInAggregateGroup(elements []interface{}, wantedAgent world.Agent) bool {
	for _, e := range elements {
		uId := e.(string)
		if uId == wantedAgent.GetUniqueId() {
			return true
		}
	}
	return false
}

func (q *AggregateQualifier) MergeAdjacentGroups(uid interface{}) {
	w := q.world
	allAdjacentGroups := make(map[string][]world.Aggregate)
	for _, g := range w.Aggregates {
		groupName := g.GetName()
		for _, key := range g.ElementKeys() {
			if key == uid {
				allAdjacentGroups[groupName] = append(allAdjacentGroups[groupName], g)
			}
		}
	}
	for _, groups := range allAdjacentGroups {
		if len(groups) > 0 {
			var primaryElements []interface{}
			for _, e := range groups[0].ElementKeys() {
				primaryElements = append(primaryElements, e)
			}
			primaryElements = lib.UnionInterface(primaryElements, []interface{}{uid})
			// If we found more than one adjacent group, this agent must be the joiner
			// Merge all the groups into the first primary group.
			if len(groups) > 1 {
				for i := 1; i < len(groups); i++ {
					redundantGroup := groups[i]
					primaryElements = lib.UnionInterface(primaryElements, redundantGroup.ElementKeys())
					// Delete the redundant group
					redundantGroup.Destroy()
				}
			}
			groups[0].SetElements(primaryElements)
		}
	}
}

func getGroupIdxById(id int, groups []world.Aggregate) int {
	for i, g := range groups {
		if g.GetId() == id {
			return i
		}
	}
	return -1
}
