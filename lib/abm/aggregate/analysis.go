package aggregate

import (
	"gitlab.com/drakonka/roee/lib/abm/world"
	"math"
	"reflect"
)

type proximity struct {
	a1       world.Agent
	a2       world.Agent
	propName string
	Distance float64
}

func getDistance(p1, p2 interface{}) (d float64) {
	switch x := p1.(type) {
	case world.Pos:
		y := p2.(world.Pos)
		a := math.Pow(float64(y.X-x.X), 2)
		b := math.Pow(float64(y.Y-x.Y), 2)
		d = math.Sqrt(a + b)
	case float64:
		y := p2.(float64)
		d = math.Abs(x - y)
	case float32:
		y := p2.(float32)
		d = float64(float32Abs(x - y))
	case int32:
		y := p2.(int32)
		d = float64(int32Abs(x - y))
	case int64:
		y := p2.(int64)
		d = float64(int64Abs(x - y))
	case int:
		y := p2.(int)
		d = float64(intAbs(x - y))
	}
	return d
}

func float32Abs(n float32) float32 {
	if n < 0 {
		return -n
	}
	return n
}

func int32Abs(n int32) int32 {
	if n < 0 {
		return -n
	}
	return n
}

func int64Abs(n int64) int64 {
	if n < 0 {
		return -n
	}
	return n
}

func intAbs(n int) int {
	if n < 0 {
		return -n
	}
	return n
}

func GetProximities(pop map[string]world.Agent, propName string) []proximity {
	var proximities []proximity
	if len(propName) == 0 {
		propName = "Pos"
	}

	allAgents := copyMap(pop)
	agentsToProcess := copyMap(allAgents)
	for p1, a1 := range agentsToProcess {
		a1.GetLock().RLock()
		r1 := reflect.ValueOf(a1)
		a1Field := reflect.Indirect(r1).FieldByName(propName)
		a1Prop := a1Field.Interface()
		a1.GetLock().RUnlock()
		agentsToProcess = copyMap(allAgents)
		for p2, a2 := range allAgents {
			if p1 == p2 {
				continue
			}
			a2.GetLock().RLock()
			r2 := reflect.ValueOf(a2)
			a2Field := reflect.Indirect(r2).FieldByName(propName)
			a2Prop := a2Field.Interface()
			a2.GetLock().RUnlock()
			dist := getDistance(a1Prop, a2Prop)
			prox := proximity{
				a1:       a1,
				a2:       a2,
				propName: propName,
				Distance: dist,
			}
			proximities = append(proximities, prox)
		}
		delete(allAgents, p1)
	}
	return proximities
}

func GetProximity(a1, a2 world.Agent, propName string) proximity {
	var prox proximity
	if len(propName) == 0 {
		propName = "Pos"
	}

	r1 := reflect.ValueOf(a1)
	a1Field := reflect.Indirect(r1).FieldByName(propName)
	a1Prop := a1Field.Interface()

	r2 := reflect.ValueOf(a2)
	a2Field := reflect.Indirect(r2).FieldByName(propName)
	a2Prop := a2Field.Interface()

	dist := getDistance(a1Prop, a2Prop)
	prox = proximity{
		a1:       a1,
		a2:       a2,
		propName: propName,
		Distance: dist,
	}
	return prox
}

func copyMap(m map[string]world.Agent) map[string]world.Agent {
	newM := make(map[string]world.Agent)
	for k, v := range m {
		newM[k] = v
	}
	return newM
}
