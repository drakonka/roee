package aggregate

import (
	"gitlab.com/drakonka/roee/lib/abm/world"

	"fmt"
	"sync"
	"time"
)

type AggregatePos477 struct {
	Age        int
	Elements   map[string]*world.Listener
	Id         int
	Name       string
	Pos        world.Pos
	Qualifiers []world.Qualifier
	lock       *sync.RWMutex
	world      *world.Grid
}

func (a *AggregatePos477) AddElement(e interface{}) {
	a.lock.Lock()
	if a.Elements == nil {
		a.Elements = make(map[string]*world.Listener)
	}
	uid := e.(string)
	agent := a.world.Population[uid]
	if agent != nil {
		l := agent.AddListener()
		a.Elements[uid] = l
		go a.listen(l)
	}
	a.lock.Unlock()
}

func (a *AggregatePos477) Destroy() {
	fmt.Printf("\n\nDestroying Aggregate %!d(MISSING)", a.Id)
	a.Age = -1
	a.world.Lock.Lock()
	idx := getGroupIdxById(a.Id, a.world.Aggregates)
	if idx > -1 {
		fmt.Printf("\nFound aggregate to destroy %!!(MISSING)d(MISSING)", a.Id)

		a.world.Aggregates = append(a.world.Aggregates[:idx], a.world.Aggregates[idx+1:]...)
		for _, key := range a.ElementKeys() {
			a.RemoveElement(key)
		}
	}
	a.world.Lock.Unlock()
}

func (a *AggregatePos477) ElementKeys() []interface{} {
	var keys []interface{}
	a.lock.RLock()
	for k := range a.Elements {
		keys = append(keys, k)
	}
	a.lock.RUnlock()
	return keys
}

func (a *AggregatePos477) GetAge() int {

	return a.Age
}

func (a *AggregatePos477) GetElements() map[string]*world.Listener {

	return a.Elements
}

func (a *AggregatePos477) GetId() int {

	return a.Id
}

func (a *AggregatePos477) GetName() string {

	return a.Name
}

func (a *AggregatePos477) GetPos() world.Pos {

	return a.Pos
}

func (a *AggregatePos477) GetQualifiers() []world.Qualifier {

	return a.Qualifiers
}

func (a *AggregatePos477) GetSize() (int, error) {

	var out0 int
	var out1 error
	return out0, out1
}

func (a *AggregatePos477) Getlock() *sync.RWMutex {

	return a.lock
}

func (a *AggregatePos477) Getworld() *world.Grid {

	return a.world
}

func (a *AggregatePos477) Init(w *world.Grid) {
	a.Name = "AggregatePos477"
	a.world = w
	w.Aggregates = append(w.Aggregates, a)
	a.lock = &sync.RWMutex{}
}

func (a *AggregatePos477) Qualify(uid interface{}) bool {
	var qualified bool
	for _, q := range a.Qualifiers {
		for _, e := range a.ElementKeys() {
			// We don't want to qualify an agent against itself...
			if e == uid {
				continue
			}
			qualified = q.QualifyAgent(e, uid)
			if qualified {
				a.AddElement(uid)
				q.MergeAdjacentGroups(uid)
				break
			} else {
				a.lock.RLock()
				_, ok := a.Elements[uid.(string)]
				a.lock.RUnlock()
				if ok {
					a.RemoveElement(uid)
					break
				}
			}
		}
	}
	if a.Elements == nil || len(a.Elements) == 0 {
		a.Destroy()
		return false
	}
	return qualified
}

func (a *AggregatePos477) RemoveElement(e interface{}) {
	uid := e.(string)
	exUids := a.ElementKeys()
	for _, exUid := range exUids {
		exUidStr := exUid.(string)
		if exUidStr == uid {
			a.lock.RLock()
			listener := a.Elements[exUidStr]
			a.lock.RUnlock()
			if listener != nil && !listener.Closed {
				select {
				case listener.C <- "removed":
				default:
				}
				listener.Destroy()
			}
			a.lock.Lock()
			delete(a.Elements, exUidStr)
			numElements := len(a.Elements)
			a.lock.Unlock()

			if numElements <= 1 && a.Age > -1 {
				a.Destroy()
			}
			break
		}
	}
}

func (a *AggregatePos477) SetElements(elements []interface{}) {
	for _, k := range a.ElementKeys() {
		var keep bool
		for idx, nE := range elements {
			if nE == k {
				keep = true
				elements = append(elements[:idx], elements[idx+1:]...)
				break
			}
		}
		if !keep {
			a.RemoveElement(k)
		}
	}
	for _, e := range elements {
		a.AddElement(e)
	}
}

func (a *AggregatePos477) SetId(param0 int) {
	a.Id = param0

	return
}

func (a *AggregatePos477) SetQualifiers(param0 []world.Qualifier) {
	a.Qualifiers = param0

	return
}
func (a *AggregatePos477) listen(l *world.Listener) {
	for {
		select {

		case val := <-l.C:
			if val == nil {
				return
			}
			msg := val.(string)
			fmt.Println("Got message: " + msg)
			switch msg {
			case "death":
				fmt.Println("Got death message from " + l.Name)
				a.RemoveElement(l.Name)
				return
			case "removed":
				fmt.Println(a.Name + " ( " + string(a.Id) + ") Got removed message from " + l.Name)
				return
			default:
				fmt.Println("Message was " + msg)
				return
			}
		}
	}
	fmt.Println("Sleeping")
	time.Sleep(time.Second)
}
