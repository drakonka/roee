package main

import "gitlab.com/drakonka/roee/lib/abm"

type Thing struct {
	id           int
	pos          abm.Pos
	age          int
	instructions []abm.Instruction
}

var Agent Thing

func (a Thing) getId() int {
	return a.id
}

func (a Thing) setId(i int) {
	a.id = i
}

func (a Thing) getPos() abm.Pos {
	return a.pos
}

func (a Thing) setPos(p abm.Pos) {
	a.pos = p
}

func (a Thing) getInstructions() []abm.Instruction {
	return a.instructions
}

func (a Thing) setInstructions(i []abm.Instruction) {
	a.instructions = i
}

func (a Thing) getAge() int {
	return a.age
}

func (a Thing) setAge(age int) {
	a.age = age
}
