package main

import (
	"fmt"
	_ "net/http/pprof"
	"gitlab.com/drakonka/roee/lib/abm/agent"
	"gitlab.com/drakonka/roee/lib/abm/world"
	"gitlab.com/drakonka/roee/lib/orm"
	"reflect"
	"runtime"
	"strings"
	"sync"

	"os/exec"
	"net/http"
	"log"
	"time"
	"gitlab.com/drakonka/roee/lib"
	"github.com/coreos/etcd/pkg/fileutil"
	"os"
	"math/rand"
)

const (
	ticks     = 50
	gridSizeX = 50
	gridSizeY = 50
	agents    = 10
	doRestart = true
)

var observers = []orm.Observer{&orm.AggregationObserver{}}

var qualifiers = []world.Qualifier{

}

func main() {
	rand.Seed(time.Now().UnixNano())
	f := createLogFile()
	defer f.Close()
	runtime.SetBlockProfileRate(1)
	go http.ListenAndServe("localhost:6060", nil)

	world.InstructionTypes = append(world.InstructionTypes, reflect.TypeOf(world.Copy{}), reflect.TypeOf(world.Del{}))
	world.AgentTypes = append(world.AgentTypes, reflect.TypeOf(agent.Thing{}))
	world.Qualifiers = append(world.Qualifiers, qualifiers...)

	var reifiers []orm.Reifier
	log.Println("Starting Roee")
	wg := &sync.WaitGroup{}
	var obsChans []chan world.Grid
	for _, obs := range observers {
		log.Printf("\nInitializing observer: %v", reflect.TypeOf(obs))
		wg.Add(1)
		obs.Init(wg)
		reifiers = append(reifiers, obs.NextStep())
		obsChans = append(obsChans, obs.GridChan())
	}

	log.Printf("\nCreating %d x %d world with %d observer channels", gridSizeX, gridSizeY, len(obsChans))
	wrld := world.NewGrid(gridSizeX, gridSizeY, obsChans)
	world.World = wrld

	log.Println("\n Initializing Qualifiers")
	for _, q := range qualifiers {
		q.Init(wrld)
	}

	log.Println("\nPopulating world")
	wrld.Populate(agents)

	log.Printf("\nRunning world for %d ticks", ticks)
	wrld.Run(ticks)

	log.Println("\nWaiting for all observers to finish")
	wg.Wait()
	log.Println("\nAll observers are done!")

	// Now start reifying

	var modifiers []orm.Modifier

	owg := &sync.WaitGroup{}
	for _, r := range reifiers {
		owg.Add(1)
		err := r.Do(owg)
		if err == nil {
			ns := r.NextStep()
			modifiers = append(modifiers, ns)
		}

	}
	owg.Wait()

	// Now start modifying
	mwg := &sync.WaitGroup{}
	for _, m := range modifiers {
		mwg.Add(1)
		m.Do(mwg)
	}
	mwg.Wait()

	if doRestart {
		root := getProjectRootPath()
		scriptPath := root + "/lib/orm/buildandrun.sh"
		log.Printf("\nscript path: %s", scriptPath)

		cmd := exec.Command(scriptPath)
		cmd.Start()
	}
	log.Println("\nDone!")
}

func getProjectRootPath() string {
	_, b, _, _ := runtime.Caller(0)
	folders := strings.Split(b, "/")
	folders = folders[:len(folders)-3]
	path := strings.Join(folders, "/")
	return path
}

func createLogFile() *os.File {
	fDir := fmt.Sprintf("%s/logs", getProjectRootPath())
	fileutil.TouchDirAll(fDir)
	fPath := fmt.Sprintf("%s/roee_%d.log", fDir, time.Now().Unix())
	f := lib.MakeAndOpenFile(fPath)
	//set output of logs to f
	log.SetOutput(f)
	os.Stderr = f
	os.Stdout = f
	return f
}