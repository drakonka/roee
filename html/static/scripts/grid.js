var gridDiv = document.getElementById("grid");


function selectCell(cellKey) {
    agent = gridState.Population[cellKey];
    agent.aggregates = [];
    if (gridState.Aggregates !== null) {
        for (i = 0; i < gridState.Aggregates.length; i++) {
            aggregate = gridState.Aggregates[i];
            if (aggregate.Elements[cellKey] !== undefined) {
                agent.aggregates.push(aggregate);
            }
        }
    }
    console.log("Aggregates:");
    console.log(agent.aggregates);
    selectionDiv = document.getElementById("selection");
    selectionDiv.innerHTML = "<h1>" + cellKey + "</h1>";
    selectionDiv.innerHTML += "<ul>";
    selectionDiv.innerHTML += "<li><strong>ID:</strong>" + agent.Id + "</li>";
    selectionDiv.innerHTML += "<li><strong>Age:</strong>" + agent.Age + "</li>";
    selectionDiv.innerHTML += "<li><strong>Aggregates:</strong>";
    for (var i = 0; i < agent.aggregates.length; i++) {
        aggregate = agent.aggregates[i];
        selectionDiv.innerHTML += aggregate.Name + "-" + aggregate.Id + ", ";
    }
    selectionDiv.innerHTML += "</li>";
    selectionDiv.innerHTML += "</ul>";

}

function generateGrid(tick) {
    var grid = document.createElement('table');
    for (var y = 0; y < gridState.Y; y++) {
        var row = grid.insertRow(y);
        for (var x = 0; x < gridState.X; x++) {
            var cell = row.insertCell(x);
            var cellKey = x + ',' + y;
            var agent = gridState.Population[cellKey];

            if (agent !== undefined) {
                // Find color
                var col = randomColor({
                    seed: agent.Id,
                    luminosity: 'bright'
                });
                agent.color = col;
                agents.push({
                    key: cellKey,
                    value: agent
                });
                cell.title = agent.Instructions[0].Name;
                cell.innerHTML = cellKey;
                cell.style.backgroundColor = col;
                if (gridState.Aggregates !== null) {
                    // See if this is part of an aggregate
                    for (var i = 0; i < gridState.Aggregates.length; i++) {
                        var stopCheck = false;
                        var aggregate = gridState.Aggregates[i];
                        for (var key in aggregate.Elements) {
                            if (key == cellKey) {
                                var col = randomColor({
                                    seed: aggregate.Id,
                                    luminosity: 'dark',
                                    hue: 'red'
                                });
                                if (aggregate.Name.includes("Pos")) {
                                    col = "green";
                                    stopCheck = true;
                                }
                                // Find color
                                cell.style.borderColor = col;
                                cell.style.borderWidth = "4px";
                                cell.style.borderStyle = "dotted";
                                cell.title += "(a: " + aggregate.Id + ")";

                            }
                        }
                        if (stopCheck === true) {
                            break;
                        }
                    }
                }
            }

        }
    }
    gridDiv.innerHTML = "Tick " + currentTick + '/' + (totalTicks - 1);
    gridDiv.appendChild(grid);

    grid.addEventListener("click",
        function(e) {
            if(e.target && e.target.nodeName === "TD") {
                selectCell(e.target.textContent);
            }
        }
    );

}
